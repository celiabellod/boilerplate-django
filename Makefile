.PHONY: bash
bash: ## Get a shell into app container
	docker-compose exec app sh

.PHONY: logs
logs: ## Get a shell into app container
	docker-compose logs -f app

.PHONY: install
install:  ## Install project
	docker-compose up --build -d

.PHONY: up
up: ## Start containers
	docker-compose up -d

.PHONY: stop
stop: ## Stop containers
	docker-compose stop

.PHONY: tests
tests:
	docker-compose run --rm app sh -c "pytest"

.PHONY: lint
lint:
	docker-compose run --rm app sh -c "flake8"

.PHONY: migration
migration:
	docker-compose run --rm app sh -c "python manage.py makemigrations"

.PHONY: migrate
migrate:
	docker-compose run --rm app sh -c "python manage.py migrate"

.PHONY: createsuperuserc
createsuperuser:
	docker-compose run --rm app sh -c "python manage.py createsuperuser"
