FROM python:3.9-alpine
ENV PYTHONUNBUFFERED 1

COPY ./app /app
WORKDIR /app
EXPOSE 8000

RUN pip install --upgrade pip
RUN pip install -r requirements.txt
RUN pip install -r requirements.dev.txt
